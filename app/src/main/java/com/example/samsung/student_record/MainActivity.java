package com.example.samsung.student_record;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    Button add1;
    ListView l1;
    Adapters adapter;
    TextView tv1;
    List<Student> data = new ArrayList<Student>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add1 = (Button) findViewById(R.id.add1);
        l1 = (ListView) findViewById(R.id.l1);
        adapter = new Adapters(this, data);

        l1.setAdapter(adapter);
        l1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this,
                        (CharSequence) adapter.data.get(position),
                        Toast.LENGTH_LONG).show();
            }
        });


        //   textView1=(TextView)findViewById(R.id.textView1);
    }

    public void clickme() {
        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
        startActivityForResult(intent, 2);
        // startActivityForResult(intent,3);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            if(resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                String roll_no = data.getStringExtra("roll_no");
                Student obj = new Student(name, roll_no);
                adapter.data.add(obj);
                adapter.notifyDataSetChanged();
            }
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.add1:
                clickme();
                break;
            default:
                break;
        }
    }
   /* public class Student {
        String name,roll_no;
        ArrayList<String> a1 = new ArrayList<String>();

        public Student(String name,String roll_no)
        {
            this.name=name;
            this.roll_no=roll_no;
        }
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
