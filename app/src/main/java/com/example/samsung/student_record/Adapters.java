package com.example.samsung.student_record;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.samsung.student_record.R;

import java.util.List;

/**
 * Created by samsung on 1/21/2015.
 */
public class Adapters extends BaseAdapter {
    public List<Student> data;
    Context ab;

    public Adapters(Context ab, List<Student> data) {
        this.ab = ab;
        this.data = data;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ab.getSystemService(ab.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list, null);
        TextView tv = (TextView) view.findViewById(R.id.t1);
        tv.setText(data.get(position).name);


        TextView tv1 = (TextView) view.findViewById(R.id.t2);
        tv1.setText(data.get(position).roll_no);
        return view;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }
}
