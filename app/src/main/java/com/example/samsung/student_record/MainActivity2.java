package com.example.samsung.student_record;

import android.content.Intent;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
public class MainActivity2 extends ActionBarActivity {

    Button b1,b2;
    EditText e1,e2;
    ListView l1;
    ArrayList<Student> a1=new ArrayList<Student>();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2);

        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b1);
        l1= (ListView)findViewById(R.id.l1);
    }
    public void calculate()
    {
        Intent intent=new Intent();
        intent.putExtra("name", e1.getText().toString());
        intent.putExtra("roll_no", e2.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }


    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.b1:
                calculate();
                break;
            case R.id.b2:
                finish();
                break;
            default:
                break;
        }
    }
}









/*

            Intent intent =getIntent();
       btn = (Button) findViewById(R.id.btn);
       bt = (Button) findViewById(R.id.bt);
       e1 = (EditText) findViewById(R.id.e1);
       e2 = (EditText) findViewById(R.id.e2);

    }
    */
/*
    public void main_screen()
    {
        Intent intent = new Intent(MainActivity2.this,MainActivity.class);
        intent.putExtra("first",btn.getText().toString());
        intent.putExtra("second",bt.getText().toString());

        startActivity(intent);
     }
     */

/*
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn:

                main_screen();
                break;
            case R.id.bt:
                finish();
                break;
            default:
                break;
        }
    }

            */
            /*
            @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

            */

